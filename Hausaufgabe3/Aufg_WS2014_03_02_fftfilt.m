% Aufg_WS2014_03_02_fftfilt.m
% Wintersemester 2014/2015
% Autor: Vladislav Okunev (v.okunev@tum.de)
%        T. Habigt
%        H. Abdellatif

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Schnittstellen I, Aufgabe 4: FFT und ihre Inverse, Filtern von Signalen 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diese Aufgabe wurde bearbeitet von:
% Name: Markus Hofbauer
% Matrikelnummer: 03623454
%
% Name: Kevin Meyer
% Matrikelnummer: 03623271
%
% Name: Thomas Markowitz
% Matrikelnummer: 03623504
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemeine Hinweise: 
% Die Aufgaben koennen in Gruppen von bis zu drei Studierenden bearbeitet
% werden. Tragen Sie dazu bitte in alle Dateien, die Sie bearbeiten, die
% Namen und Matrikelnummern der Gruppenmitglieder ein. Pro Gruppe muss die
% Hausaufgabe nur einmal abgegeben werden. Werden mehrere Loesungen
% abgegeben, wird die neueste Abgabe bewertet.
% 
% Bitte bearbeiten Sie die Aufgaben direkt in den *.m-Dateien der Angabe.
% Bitte aendern Sie den Dateinamen nicht, um die Zuordnung zu erleichtern.
% Verpacken Sie die bearbeiteten Aufgaben bitte wieder als zip-Archiv. In
% diesem Archiv muessen alle Dateien enthalten sein, die zur Ausfuehrung
% der Skripte noetig sind (keine Autosave *.asv oder *.m~ Dateien).
% 
% Die Loesungen muessen bis zur jeweils angegebenen Deadline in Moodle
% (https://www.moodle.tum.de) hochgeladen werden. Wenn es ein Problem
% bei der Abgabe der Hausaufgaben gab, werden Sie per E-Mail
% benachrichtigt.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 1) Laden Sie die Datei 'SigSpec.mat'. Diese enth???lt das einseitige(!) 
% Spektrum 'singleSided' des Zeitsignals 'signal' mit Fs = 1kHz.

% 2) Berechnen Sie das vollst???ndige Spektrum des Zeitsignals mit Hilfe der 
% Funktion [fft]. (Hinweise dazu finden Sie in der Hilfe zu fft).
% Vergleichen Sie das vollst???ndige Spektrum mit dem einseitigen Spektrum
% aus 1): welche Symmetrieeigenschaften des vollst???ndigen Spektrums 
% erkennen Sie? Rekonstruieren Sie das vollst???ndige Spektrum aus dem 
% einseitigen.
% Hinweis: betrachten Sie das gespiegelte einseitige Spektrum [fliplr]. 

% 3) Berechnen Sie den zum einseitigen Spektrum geh???renden Frequenzvektor.
   
% 4) Rekonstruieren Sie das Zeitsignal aus dem einseitigen Spektrum [ifft].
% Plotten Sie das urspr???ngliche Zeitsignal aus 'SigSpec.mat', das 
% rekonstruierte Zeitsignal sowie ihre Differenz in jeweils einem
% Subplot(vertikal angeordnet). Berechnen sie hierf???r einen geeigneten 
% Zeitvektor mit t(1)=0 und Fs=1000 Hz. Beschriften Sie die Plots und die 
% Achsen.
% Gleichen sich das urps???ngliche und das rekonstruierte Signal exakt? 
% Begr???nden Sie das beobachtete Verhalten.

% 5) Setzen Sie alle Frequenzanteile mit f > 2 Hz im einseitigen 
% Spektrum auf Null und berechnen Sie das Zeitsignal zu diesem Spektrum.
% Stellen Sie die Signale in einer neuen Figure dar: plotten Sie im ersten
% Subplot dieses Zeitsignal und das Zeitsignal aus 'SigSpec.mat';
% plotten Sie im zweiten Subplot (Anordnung vertikal) die entsprechenden
% Spektren im Frequenzbereich 0 Hz < f < 4 Hz. Denken Sie an die Subplot-
% Titel, Achsenbeschriftungen und Legende. Hinweise zur Darstellung von
% Spektren finden Sie in der Hilfe zur fft.
% Um welche Art von Filter handelt es sich hierbei? (Tief-,Band-, Hochpass)

%% L??sung
close all;
clear all;
clc;

%% Aufgabe 1
display('Aufgabe 1')
display('Laden der Datei SigSpec.mat.')
load('SigSpec.mat')
disp(' ')

%% Aufgabe 2
display('Aufgabe 2')

Fs = 1e3;
L = length(signal);
NFFT = 2^nextpow2(L);
Y = fft(signal,NFFT)/L;
fFull = Fs*linspace(0,1,NFFT);
fSingle = Fs/2*linspace(0,1,NFFT/2+1);

fig = figure;
fig.Name = 'Aufgabe 2';
fig.NumberTitle = 'off';

axe(1) = subplot(3,1,1);
plot(fFull,abs(Y))
title('Vollst??ndiges Spektrum')
ylabel('|Y(f)|')

axe(2) = subplot(3,1,2);
plot(fSingle,abs(singleSided),'r')
title('Einseitiges Spektrum')
ylabel('|Y(f)|')

axe(3) = subplot(3,1,3);
yRek = [singleSided,fliplr(singleSided)];
plot([fSingle,fSingle+fSingle(end)],abs(yRek),'r')
title('Rekonstruiertes Spektrum')
xlabel('f [Hz]')
ylabel('|Y(f)|')

linkaxes(axe);

display('Das Vollst??ndige Spektrum ist symmetrisch zur Frequenz 500Hz.')
disp(' ')

%% Aufgabe 3
display('Aufgabe 3')
display('Der Frequenzvektor wurde in Aufgabe 2 zum plotten berechnet und ist in der Variable fSingle gespeichert.')
disp(' ')

%% Aufgabe 4
display('Aufgabe 4')

t = 1e-3*(0:length(signal)-1);
signalRek = ifft(Y,NFFT)*L;

fig2 = figure;
fig2.Name = 'Aufgabe 4';
fig2.NumberTitle = 'off';

axe2(1) = subplot(3,1,1);
plot(t,signal)
title('Urspr??ngliches Zeitsingal')
ylabel('y(t)')

axe2(2) = subplot(3,1,2);
plot(t,signalRek)
title('Rekonstruiertes Zeitsingal')
ylabel('y(t)')

axe2(3) = subplot(3,1,3);
plot(t,signalRek - signal)
title('Differenz')
xlabel('t [s]')
ylabel('\Delta')

linkaxes(axe2,'x');
xlim(axe2(1),[0,8.5])

display('Nein, das urspruengliche Signal und das rekonstruierte Signal sind nicht exakt gleich.')
display('Sie unterscheiden sich allerdings nur in der Groessenordnung von e-16. Dies ist vernschlaessigbar klein und mit der Rechengenauigkeit von Matlab (eps = 2.2204e-16) zu erklaeren.')
disp(' ')

%% Aufgabe 5
display('Aufgabe 5')

filtered = singleSided;
filtered(fSingle > 2) = 0;
signalFilteredRek = ifft(filtered,NFFT,'symmetric')*L;

fig3 = figure;
fig3.Name = 'Aufgabe 5';
fig3.NumberTitle = 'off';

axe3(1) = subplot(2,1,1);
hold on;
plot(t,signal,'DisplayName','signal')
plot(t,signalFilteredRek,'DisplayName','filtered')
hold off;
xlim(axe3(1),[0,8.5])
title('Zeitsingale')
ylabel('y(t)')
xlabel('t [s]')
legend(axe3(1),'show');

axe3(2) = subplot(2,1,2);
hold on;
plot(fSingle,abs(singleSided),'DisplayName','single')
plot(fSingle,abs(filtered),'DisplayName','filtered')
hold off;
title('Gefiltertes Spektrum')
xlabel('f [Hz]')
ylabel('|Y(f)|')
xlim(axe3(2),[0,4])
legend(axe3(2),'show');

display('Hierbei handelt es sie um einen Tiefpass.')
display('Dieser entfernt das Rauschen (vgl. Figure 3).')