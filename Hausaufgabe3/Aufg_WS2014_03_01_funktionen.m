% Aufg_WS2014_03_01_funktionen.m
% Wintersemester 2014/2015
% Autor: Vladislav Okunev (v.okunev@tum.de)
%        T. Habigt
%        H. Abdellatif

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Programmiertechnik I, Aufgabe 1: Programmierung von Funktionen als
% m-Dateien
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diese Aufgabe wurde bearbeitet von:
% Name: Markus Hofbauer
% Matrikelnummer: 03623454
%
% Name: Kevin Meyer
% Matrikelnummer: 03623271
%
% Name: Thomas Markowitz
% Matrikelnummer: 03623504
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemeine Hinweise: 
% Die Aufgaben koennen in Gruppen von bis zu drei Studierenden bearbeitet
% werden. Tragen Sie dazu bitte in alle Dateien, die Sie bearbeiten, die
% Namen und Matrikelnummern der Gruppenmitglieder ein. Pro Gruppe muss die
% Hausaufgabe nur einmal abgegeben werden. Werden mehrere Loesungen
% abgegeben, wird die neueste Abgabe bewertet.
% 
% Bitte bearbeiten Sie die Aufgaben direkt in den *.m-Dateien der Angabe.
% Bitte aendern Sie den Dateinamen nicht, um die Zuordnung zu erleichtern.
% Verpacken Sie die bearbeiteten Aufgaben bitte wieder als zip-Archiv. In
% diesem Archiv muessen alle Dateien enthalten sein, die zur Ausfuehrung
% der Skripte noetig sind (keine Autosave *.asv oder *.m~ Dateien).
% 
% Die Loesungen muessen bis zur jeweils angegebenen Deadline in Moodle
% (https://www.moodle.tum.de) hochgeladen werden. Wenn es ein Problem
% bei der Abgabe der Hausaufgaben gab, werden Sie per E-Mail
% benachrichtigt.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 1) Programmieren Sie eine m-Funktion "distanz.m" (extra m-Datei), die den 
% Abstand von Punkten zum Ursprung berechnet, deren x- und y-Koordinaten 
% mit dem einem Array [2xN] oder [Nx2] ???bergeben werden. Ausgabe soll 
% entsprechend der Dimension des Inputs als [1xN] oder [Nx1]-Vektor erfolgen.
% Die Benutzung von Funktionen aus Toolboxen ist nicht zul???ssig.

% 2) Programmieren Sie eine Funktion "rechner.m", die zwei Arrays und einen
% String als Parameter akzeptiert. Der String soll die Rechenoperation
% definieren, die auf die Arrays angewendet wird: 'addition', 'subtraktion',
% 'multiplikation' oder 'division' [switch].
% Wird die Funktion mit einem Ausgabeparameter aufgerufen, soll das Ergebnis
% der Rechenoperation (elementweise angewandt auf die Arrays) zur???ckgegeben 
% werden. Wird kein Ausgabeparameter spezifiziert, soll stattdessen das 
% Ergebnis auf der Konsole ausgegeben werden [nargout, disp].

% 3) Programmieren Sie eine Funktion "strgen.m", die beliebig viele Skalare
% oder Strings als Parameter akzeptiert, sie zu einem einzigen String
% zusammenf???gt (durch Leerzeichen getrennt) und zur???ckgibt.
% [varargin, num2str, cellfun, strjoin]. 

%% L??sung
close all;
clear all;
clc;

%% Aufgabe 1
display('Aufgabe 1')
display('L??sung in der Datei distanz.m.')
display('Da in der Angabe kein genaueres Vorgehen beschrieben wird, wird f??r den Fall des Eingabevektors der Gr????e [2x2] das Ergebnis als [1x2] Vektor ausgegeben.')
ziel = [1,0; 1,1; 2,2; 4,5]
distanzen = distanz(ziel)
disp(' ')

%% Aufgabe 2
display('Aufgabe 2')
display('L??sung in der Datei rechner.m.')
array1 = [1,0]
array2 = [3,2]
display('Type: addition')
ergebnis = rechner(array1, array2, 'addition')
disp(' ')

%% Aufgabe 3
display('Aufgabe 3')
display('L??sung in der Datei strgen.m.')
ergebnis = strgen('Das', 'ist', 'ein', 'Test', 'fuer Aufgabe', 3)

clear