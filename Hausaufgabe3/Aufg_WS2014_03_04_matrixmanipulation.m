% Aufg_WS2014_03_04_matrixmanipulation.m
% Wintersemester 2014/2015
% Autor: Vladislav Okunev (v.okunev@tum.de)
%        T. Habigt
%        H. Abdellatif

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Schnittstellen I, Aufgabe 2: Manipulation von Matrixelementen mit reshape 
% und cat, Char/ASCII-Code-Manipulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diese Aufgabe wurde bearbeitet von:
% Name: Markus Hofbauer
% Matrikelnummer: 03623454
%
% Name: Kevin Meyer
% Matrikelnummer: 03623271
%
% Name: Thomas Markowitz
% Matrikelnummer: 03623504
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemeine Hinweise: 
% Die Aufgaben koennen in Gruppen von bis zu drei Studierenden bearbeitet
% werden. Tragen Sie dazu bitte in alle Dateien, die Sie bearbeiten, die
% Namen und Matrikelnummern der Gruppenmitglieder ein. Pro Gruppe muss die
% Hausaufgabe nur einmal abgegeben werden. Werden mehrere Loesungen
% abgegeben, wird die neueste Abgabe bewertet.
% 
% Bitte bearbeiten Sie die Aufgaben direkt in den *.m-Dateien der Angabe.
% Bitte aendern Sie den Dateinamen nicht, um die Zuordnung zu erleichtern.
% Verpacken Sie die bearbeiteten Aufgaben bitte wieder als zip-Archiv. In
% diesem Archiv muessen alle Dateien enthalten sein, die zur Ausfuehrung
% der Skripte noetig sind (keine Autosave *.asv oder *.m~ Dateien).
% 
% Die Loesungen muessen bis zur jeweils angegebenen Deadline in Moodle
% (https://www.moodle.tum.de) hochgeladen werden. Wenn es ein Problem
% bei der Abgabe der Hausaufgaben gab, werden Sie per E-Mail
% benachrichtigt.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Benutzung von Funktionen, die Teil von Toolboxen sind, ist nicht zul???ssig.
% Soweit nicht anders angegeben, sind Eingabeparameter von Funktionen als
% Zeilenvektoren zu realisieren.

% 1) Programmieren Sie eine Funktion "komb.m", die alle Kombinationen
% von Eintr???gen der beiden ???bergebenen Vektoren berechnet und 
% in einer Matrix zur???ckgibt (ohne Ber???cksichtigung der Reihenfolge). 
% Bsp.: a = komb([1 2], [3 4]) => a = [1 3; 1 4; 2 3; 2 4]

% 2) Programmieren Sie eine Verallgemeinerung dieser Funktion "komb2.m", 
% die die m???glichen Kombinationen von Eintr???gen beliebig vieler Vektoren 
% berechnet. Benutzen Sie hierf???r die Funktion [ndgrid]. Fangen Sie mit dem
% Beispiel [ [a, b] = ndgrid([1 2 3],[4 5]) ] an. [varargin, reshape, cat]
% Tipp: Sie k???nnen mehrere R???ckgabewerte einer Funktion in einem Cell Array
% speichern, z.B.: a = cell(N,1);  [a{:}] = ndgrid(x1, x2, ..., xN);
% Benutzen Sie keine Schleifen.

% 3) Programmieren Sie eine Funktion "xlsIndex.m", die einen Integer-
% Wertigen Index (Nat???rliche Zahl) in einen Char-Wertigen Index ???bersetzt, 
% wie diese von MS Excel benutzt werden: 
% 1=>A, 26=>Z, 27=>AA, 28=> AB, 53=>AB u.s.w.
% Tipp: Sie k???nnen [dec2base] und ASCII-Code-Manipulation benutzen (nicht
% zwingend)

%% L??sung
close all;
clear all;
clc;

%% Aufgabe 1
display('Aufgabe 1')
display('Loesung in der Datei komb.m')
a = [1,2,3]
b = [4,5]
kombinationen = komb(a,b)
disp(' ')

%% Aufgabe 2
display('Aufgabe 2')
display('Loesung in der Datei komb2.m')
display(a)
display(b)
c = [7,8,9]
kombinationen = komb2(a,b,c)
disp(' ')

%% Aufgabe 3
display('Aufgabe 3')
display('Loesung in der Datei xlsIndex.m')
display('Test:')
disp(['Eingabe: 1     <->    A |', 'Ausgabe: ', xlsIndex(1)])
disp(['Eingabe: 26    <->    Z |', 'Ausgabe: ', xlsIndex(26)])
disp(['Eingabe: 27    <->   AA |', 'Ausgabe: ', xlsIndex(27)])
disp(['Eingabe: 676   <->   YZ |', 'Ausgabe: ', xlsIndex(676)])
disp(['Eingabe: 677   <->   ZA |', 'Ausgabe: ', xlsIndex(677)])
disp(['Eingabe: 702   <->   ZZ |', 'Ausgabe: ', xlsIndex(702)])
disp(['Eingabe: 703   <->  AAA |', 'Ausgabe: ', xlsIndex(703)])
disp(['Eingabe: 18278 <->  ZZZ |', 'Ausgabe: ', xlsIndex(18278)])
disp(['Eingabe: 18279 <-> AAAA |', 'Ausgabe: ', xlsIndex(18279)])
disp(['Eingabe: 18280 <-> AAAB |', 'Ausgabe: ', xlsIndex(18280)])
