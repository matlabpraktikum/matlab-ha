% Aufg_WS2014_04_01_Anonymous_Function.m
% Wintersemester 2014/2015
% Author: H. Abdellatif (houssem.abdellatif@ldv.ei.tum.de)
%                       (houssem.abdellatif@itk-engineering.de)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Programmiertechnik I, Aufgabe 1: Anonyme Funkionen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diese Aufgabe wurde bearbeitet von:
% Name: Markus Hofbauer
% Matrikelnummer: 03623454
%
% Name: Kevin Meyer
% Matrikelnummer: 03623271
%
% Name: Thomas Markowitz
% Matrikelnummer: 03623504
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemeine Hinweise: 
% Die Aufgaben koennen in Gruppen von bis zu drei Studierenden bearbeitet
% werden. Tragen Sie dazu bitte in alle Dateien, die Sie bearbeiten, die
% Namen und Matrikelnummern der Gruppenmitglieder ein. Pro Gruppe muss die
% Hausaufgabe nur einmal abgegeben werden. Werden mehrere Loesungen
% abgegeben, wird die neueste Abgabe bewertet.
% 
% Bitte bearbeiten Sie die Aufgaben direkt in den *.m-Dateien der Angabe.
% Bitte aendern Sie den Dateinamen nicht, um die Zuordnung zu erleichtern.
% Verpacken Sie die bearbeiteten Aufgaben bitte wieder als zip-Archiv. In
% diesem Archiv muessen alle Dateien enthalten sein, die zur Ausfuehrung
% der Skripte noetig sind (keine Autosave *.asv oder *.m~ Dateien).
% 
% Die Loesungen muessen bis zur jeweils angegebenen Deadline in Moodle
% (https://www.moodle.tum.de) hochgeladen werden. Wenn es ein Problem
% bei der Abgabe der Hausaufgaben gab, werden Sie per E-Mail
% benachrichtigt.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 1) Erzeugen Sie fuer folgende mathematische Funktionen anonyme Funktionen
% unter Matlab
% f(x) = sqrt(x)
% g(x) = a*x^2 + b*x + c
% h(x) = sin(2*pi*x + pi/2);


% 2) Programmieren Sie eine Funktion plot_my_function, die einen 
% Wertebereich von x und einen function_handle f_h einer beliebigen
% Funktion als Input-Argumente erhaelt und den Verlauf der entsprechenden
% Funktion ueber dem uebergebenen Wertebereich graphisch darstellt.

% Validieren Sie hierzu  plot_my_function fuer f, g, h ueber einen 
% beispielhaften Wertebereich von x Ihrer Wahl. Erzeugen Sie zwei
% unterschiedliche Plots von g f???r unterschiedliche Parameter a, b, c.

%% L??sung
close all;
clear all;
clc;

%% Aufgabe 1
display('Aufgabe 1')
display('Erzeuge anonyme Funktionen:')

f = @(x) sqrt(x)

gPar = @(x,a,b,c) a*x.^2 + b*x + c
g = @(x) gPar(x,1,1,1)

h = @(x) sin(2*pi*x + pi/2)

disp(' ')

%% Aufgabe 2
display('Aufgabe 2')
display('Anlegen von x mit Wertebereich von 0 bis 3.')
x = 0:0.05:3;
plot_my_function(x,f);

plot_my_function(x,g);
title('x² + x + 1')
plot_my_function(x,@(x)gPar(x,-3,0.3,1));
title('-3x² + 0.3x + 1')

plot_my_function(x,h);