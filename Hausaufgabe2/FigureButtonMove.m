function FigureButtonMove(h,~)
data = guidata(h);
xy = get(gca,'CurrentPoint');
if data{1}
    if ~isempty(data{2})
        line([xy(1,1);data{2}(1,1)], [xy(1,2), data{2}(1,2)])
    end
    data{2} = xy;
    guidata(h,data);
end
end

