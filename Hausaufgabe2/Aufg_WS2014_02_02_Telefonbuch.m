% Aufg_WS2014_02_02_Telefonbuch.m
% Wintersemester 2014/2015
% Autor: Tim Habigt (tim@tum.de)
%        H. Abdellatif

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Einfuehrung II, Aufgabe 2: Ein kleines "Telefonbuch"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diese Aufgabe wurde bearbeitet von:
% Name: Markus Hofbauer
% Matrikelnummer: 03623454
%
% Name: Kevin Meyer
% Matrikelnummer: 03623271
%
% Name: Thomas Markowitz
% Matrikelnummer: 03623504
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemeine Hinweise: 
% Die Aufgaben koennen in Gruppen von bis zu drei Studierenden bearbeitet
% werden. Tragen Sie dazu bitte in alle Dateien, die Sie bearbeiten, die
% Namen und Matrikelnummern der Gruppenmitglieder ein. Pro Gruppe muss die
% Hausaufgabe nur einmal abgegeben werden. Werden mehrere Loesungen
% abgegeben, wird die neueste Abgabe bewertet.
% 
% Bitte bearbeiten Sie die Aufgaben direkt in den *.m-Dateien der Angabe.
% Bitte aendern Sie den Dateinamen nicht, um die Zuordnung zu erleichtern.
% Verpacken Sie die bearbeiteten Aufgaben bitte wieder als zip-Archiv. In
% diesem Archiv muessen alle Dateien enthalten sein, die zur Ausfuehrung
% der Skripte noetig sind (keine Autosave *.asv oder *.m~ Dateien).
% 
% Die Loesungen muessen bis zur jeweils angegebenen Deadline in Moodle
% (https://www.moodle.tum.de) hochgeladen werden. Wenn es ein Problem
% bei der Abgabe der Hausaufgaben gab, werden Sie per E-Mail
% benachrichtigt.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 1) Definieren Sie ein Telefonbuch als struct-Array und fuegen Sie Felder
% fuer Vorname, Nachname, Nummer und Wohnort ein.

% 2) Tragen Sie drei Personen ein.

% 3) Listen Sie die Eintraege (ohne Wohnort) im "Command Window" auf (eine
% Zeile pro Eintrag).

% 4) Sortieren Sie die Eintraege nach Nachnamen und geben Sie das
% Telefonbuch erneut als Tabelle wie in 3) aus.

% 5) Speichern Sie das Telefonbuch in einer mat-Datei ab.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% L??sung
close all;
clear all;
clc;

%% Aufgabe 1
display('Aufgabe 1')
Telefonbuch = struct('Vorname', {}, 'Nachname', {}, 'Nummer', {}, 'Wohnort', {})
disp(' ')

%% Aufgabe 2
display('Aufgabe 2')
Telefonbuch(1).Vorname = 'John';
Telefonbuch(1).Nachname = 'Doe';
Telefonbuch(1).Nummer = '01234567';
Telefonbuch(1).Wohnort = 'Mordor, Mittelerde';

Telefonbuch(2).Vorname = 'Foo';
Telefonbuch(2).Nachname = 'Bar';
Telefonbuch(2).Nummer = '7';
Telefonbuch(2).Wohnort = 'Fubar, Baz qux';

Telefonbuch(3).Vorname = 'Alice';
Telefonbuch(3).Nachname = 'Frank';
Telefonbuch(3).Nummer = '666666';
Telefonbuch(3).Wohnort = 'Rabbithole, Wonderland';
display('3 Eintr?ge hinzugef?gt.')
disp(' ')

%% Aufgabe 3
display('Aufgabe 3')
tableStruct = rmfield(Telefonbuch, 'Wohnort');
struct2table(tableStruct)
disp(' ')

%% Aufgabe 4
display('Aufgabe 4')
[~,order] = sortrows(char(Telefonbuch(:).Nachname));
Telefonbuch = Telefonbuch(order);
struct2table(tableStruct(order))
disp(' ')

%% Aufgabe 5
display('Aufgabe 5')
save('Telefonbuch.mat', 'Telefonbuch');
display('In Datei gespeichert.')
disp(' ')
