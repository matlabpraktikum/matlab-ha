function FigureClickUpCallback(h,~)
data = guidata(h);
b = get(h,'selectiontype');
if strcmpi(b,'normal')
    data{1} = 0;
    data{2} = [];
    guidata(h,data);
end
end