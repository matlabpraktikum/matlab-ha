function [ charIndex ] = xlsIndex( inIntegerIndex )

dividend = inIntegerIndex;
charIndex = [];

while dividend > 0
   modulo = mod((dividend - 1),26);
   charIndex = [char(65 + modulo),charIndex];
   dividend = floor((dividend - modulo) / 26);
end

end

