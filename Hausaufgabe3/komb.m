function [ combinations ] = komb( vec1, vec2 )

[grid1, grid2] = ndgrid(vec1, vec2);
combinations = [grid1(:), grid2(:)];

end

