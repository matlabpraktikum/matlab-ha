function plot_my_function( x, f_h )

figure;
plot(x,f_h(x))

name = func2str(f_h);
name = strrep(name, '.', '');
title(['f(x) = ',name(5:end)])
xlabel('x')
ylabel('f(x)')

end

