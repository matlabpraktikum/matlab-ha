function [ d ] = distanz( xy )
%DISTANZ Berechnet Abstand von Punkten zum Ursprung
%   x- und y-Koordinaten werden mit dem einem Array [2xN] oder [Nx2] 
% übergeben.
% Ausgabe des Inputs als [1xN] oder [Nx1]-Vektor.

if size(xy,1) ~= 2
    d = sqrt(xy(:,1).^2 + xy(:,2).^2);
else
    % Da in der Angabe kein genaueres Vorgehen beschrieben wird, wird für
    % den Fall des Eingabevektors der Größe [2x2] das Ergebnis als [1x2]
    % Vektor ausgegeben
    d = sqrt(xy(1,:).^2 + xy(2,:).^2);
end

end

