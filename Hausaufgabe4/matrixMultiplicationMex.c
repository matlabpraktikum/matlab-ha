#include <mex.h>

double* matrixMultiplication(double *A, double *B, unsigned int n)
{   
    double *c = (double*) mxMalloc(n*n*sizeof(double));
    int i,k,j;
    
    for(i = 0 ; i < n ; i++ )
        for(k = 0 ; k < n ; k++ ) {
            c[i + k*n] = 0;
            for(j = 0 ;  j < n ; j++ )
                c[i + k*n] += A[i + j*n] * B[k*n + j];
        }
    
    return c;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	double *A, *B;
	double *result;
    unsigned int dimension;

    mwSize aRows = mxGetM(prhs[0]);
    mwSize aCols = mxGetN(prhs[0]);
    mwSize bRows = mxGetM(prhs[1]);
    mwSize bCols = mxGetN(prhs[1]);
	if (aRows != aCols || bRows != bCols) {
		mexErrMsgTxt("Beide Eingangsargumente muessen quadratisch sein!");
	}
    if (aRows != bRows) {
		mexErrMsgTxt("Beide Eingangsargumente muessen die gleiche Dimension besitzten!");
	}
    
	A = mxGetPr(prhs[0]);
	B = mxGetPr(prhs[1]);

    dimension = (unsigned int) aRows;
    result = matrixMultiplication(A, B, dimension);
    
    mxArray *C = mxCreateDoubleMatrix(dimension, dimension, mxREAL);
    mxSetPr(C, result);
    
    plhs[0] = C;
}
