% Aufg_WS2014_01_05_Pathlen.m
% Wintersemester 2014/2015
% Autor: Tim Habigt (tim@tum.de)
%        H. Abdellatif

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Einfuehrung I, Aufgabe 5: Berechnung der Weglaenge aus GPS-Daten
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diese Aufgabe wurde bearbeitet von:
% Name: Markus Hofbauer
% Matrikelnummer: 03623454
%
% Name: Kevin Meyer
% Matrikelnummer: 03623271
%
% Name: Thomas Markowitz
% Matrikelnummer: 03623504
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemeine Hinweise: 
% Die Aufgaben koennen in Gruppen von bis zu drei Studierenden bearbeitet
% werden. Tragen Sie dazu bitte in alle Dateien, die Sie bearbeiten, die
% Namen und Matrikelnummern der Gruppenmitglieder ein. Pro Gruppe muss die
% Hausaufgabe nur einmal abgegeben werden. Werden mehrere Loesungen
% abgegeben, wird die neueste Abgabe bewertet.
% 
% Bitte bearbeiten Sie die Aufgaben direkt in den *.m-Dateien der Angabe.
% Bitte aendern Sie den Dateinamen nicht, um die Zuordnung zu erleichtern.
% Verpacken Sie die bearbeiteten Aufgaben bitte wieder als zip-Archiv. In
% diesem Archiv muessen alle Dateien enthalten sein, die zur Ausfuehrung
% der Skripte noetig sind (keine Autosave *.asv oder *.m~ Dateien).
% 
% Die Loesungen muessen bis zur jeweils angegebenen Deadline in Moodle
% (https://www.moodle.tum.de) hochgeladen werden. Wenn es ein Problem
% bei der Abgabe der Hausaufgaben gab, werden Sie per E-Mail
% benachrichtigt.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 1) Laden Sie die Datei path_coord.txt in eine Variable [load].  Welche
% Dimensionen hat die Variable?

% 2) Interpretieren Sie die Spalten der Variable aus 1) zuerst als
% kartesische x- und y-Koordinaten von Punkten entlang eines Weges und
% stellen Sie diesen grafisch dar [plot]. Welche Laenge hat der Weg?
% (Annahmen: lineare Verbindung aufeinanderfolgender Punkte, Einheit [km])

% 3) Interpretieren Sie die Spalten der Variable aus 1) nun als
% geografische Koordinaten (Breite und Laenge) von Punkten entlang eines
% Weges.  Welche Laenge (in [km]) hat der Weg dann? (Annahmen: lineare
% Verbindung aufeinanderfolgender Punkte, Erdradius konstant 6371 km)
% Rechnen Sie dazu die geographischen Koordinaten in dreidimensionale
% kartesische Koordinaten um und summieren Sie die Abst??nde der
% aufeinanderfolgenden Punkte.

%% HINWEIS
% Die Funktion "distance" darf in dieser Aufgabe NICHT verwendet werden, 
% da diese kein Bestandteil des Standard Matlab-Paket ist,
% sondern eine Funktion der Neural Network Toolbox.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% L??sung
close all;
clear all;
clc;

%% Aufgabe 1
display('Aufgabe 1')
path_coord = load('path_coord.txt');
dim = size(path_coord);
display(['Dimension der Variable: ', num2str(dim(1)), ' x ', num2str(dim(2))])
disp(' ')

%% Aufgabe 2
display('Aufgabe 2')
figure('name', 'Aufgabe 2')
plot(path_coord(:,1), path_coord(:,2));
dist = sqrt((path_coord(2:end,1) - path_coord(1:end-1,1)).^2 + (path_coord(2:end,2) - path_coord(1:end-1,2)).^2);
display(['Gesammte Wegstrecke: ' num2str(sum(dist)), ' km'])
disp(' ')

%% Aufgabe 3
display('Aufgabe 3')
r = 6371;
lat = path_coord(:,1);
long = path_coord(:,2);

x = r*sind(lat).*cosd(long);
y = r*sind(lat).*sind(long);
z = r*cosd(lat);

dist = sqrt((x(2:end) - x(1:end-1)).^2 + (y(2:end) - y(1:end-1)).^2 + (z(2:end) - z(1:end-1)).^2);
display(['Gesammte Wegstrecke in Geokoordinaten: ' num2str(sum(dist)), ' km'])
display('Ein sch?ner Radweg vom Deutschen Museum zum Tegernsee!')