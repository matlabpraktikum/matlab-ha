% Aufg_WS2014_04_03_IO_Ascii.m
% Wintersemester 2014/2015
% Autor: Tim Habigt (tim@tum.de)
%        H. Abdellatif

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Schnittstellen: IO-Funktion
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diese Aufgabe wurde bearbeitet von:
% Name: Markus Hofbauer
% Matrikelnummer: 03623454
%
% Name: Kevin Meyer
% Matrikelnummer: 03623271
%
% Name: Thomas Markowitz
% Matrikelnummer: 03623504
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemeine Hinweise: 
% Die Aufgaben koennen in Gruppen von bis zu drei Studierenden bearbeitet
% werden. Tragen Sie dazu bitte in alle Dateien, die Sie bearbeiten, die
% Namen und Matrikelnummern der Gruppenmitglieder ein. Pro Gruppe muss die
% Hausaufgabe nur einmal abgegeben werden. Werden mehrere Loesungen
% abgegeben, wird die neueste Abgabe bewertet.
% 
% Bitte bearbeiten Sie die Aufgaben direkt in den *.m-Dateien der Angabe.
% Bitte aendern Sie den Dateinamen nicht, um die Zuordnung zu erleichtern.
% Verpacken Sie die bearbeiteten Aufgaben bitte wieder als zip-Archiv. In
% diesem Archiv muessen alle Dateien enthalten sein, die zur Ausfuehrung
% der Skripte noetig sind (keine Autosave *.asv oder *.m~ Dateien).
% 
% Die Loesungen muessen bis zur jeweils angegebenen Deadline in Moodle
% (https://www.moodle.tum.de) hochgeladen werden. Wenn es ein Problem
% bei der Abgabe der Hausaufgaben gab, werden Sie per E-Mail
% benachrichtigt.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Aus der Datei Messung.dat sollen die drei Signale t, sig1 und sig2
% importiert und grafisch dargestellt werden.
% Schreiben Sie eine entsprechende Funktion und verwenden Sie zum Lesen der
% Daten ausschliesslich den Befehl fread.

%% L??sung
close all;
clear all;
clc;

%% Aufgabe
display('Aufgabe')
fid = fopen('Messung.dat','r');
A = fread(fid, '*char');
fclose(fid);

A = A(62:end-1);
B = reshape(A, 28, [])';

[nrows, ncols] = size(B);
nums = zeros(nrows, 3);
for k = 1:nrows
    nums(k,:) = sscanf(B(k,:), '%lf %lf %lf');
end;

t = nums(:,1);
sig1 = nums(:,2);
sig2 = nums(:,3);

figure;
hold on;
plot(t,sig1,'DisplayName','Signal 1');
plot(t,sig2,'DisplayName','Signal 2');
hold off;

title('Visual: Messung.dat')
xlabel('t')
legend('show')
