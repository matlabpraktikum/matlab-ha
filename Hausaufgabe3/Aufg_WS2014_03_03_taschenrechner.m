% Aufg_WS2014_03_03_taschenrechner.m
% Wintersemester 2014/2015
% Autor: Vladislav Okunev (v.okunev@tum.de)
%        T. Habigt
%        H. Abdellatif

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Schnittstellen I, Aufgabe 3: Programmierung eines Taschenrechners
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diese Aufgabe wurde bearbeitet von:
% Name: Markus Hofbauer
% Matrikelnummer: 03623454
%
% Name: Kevin Meyer
% Matrikelnummer: 03623271
%
% Name: Thomas Markowitz
% Matrikelnummer: 03623504
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemeine Hinweise: 
% Die Aufgaben koennen in Gruppen von bis zu drei Studierenden bearbeitet
% werden. Tragen Sie dazu bitte in alle Dateien, die Sie bearbeiten, die
% Namen und Matrikelnummern der Gruppenmitglieder ein. Pro Gruppe muss die
% Hausaufgabe nur einmal abgegeben werden. Werden mehrere Loesungen
% abgegeben, wird die neueste Abgabe bewertet.
% 
% Bitte bearbeiten Sie die Aufgaben direkt in den *.m-Dateien der Angabe.
% Bitte aendern Sie den Dateinamen nicht, um die Zuordnung zu erleichtern.
% Verpacken Sie die bearbeiteten Aufgaben bitte wieder als zip-Archiv. In
% diesem Archiv muessen alle Dateien enthalten sein, die zur Ausfuehrung
% der Skripte noetig sind (keine Autosave *.asv oder *.m~ Dateien).
% 
% Die Loesungen muessen bis zur jeweils angegebenen Deadline in Moodle
% (https://www.moodle.tum.de) hochgeladen werden. Wenn es ein Problem
% bei der Abgabe der Hausaufgaben gab, werden Sie per E-Mail
% benachrichtigt.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% In dieser Aufgabe soll ein einfacher Taschenrechner programmiert werden.
% Die Datei 'taschenrechner.p' zeigt, wie der Taschenrechner aussehen soll.
% Implementieren Sie die Funktionalit???t dieses Beispiels nach.
%
% Achten Sie auf folgende Eigenschaften:
% - Gr??????e der Figure ist nicht ???nderbar
% - Toolbar ist ausgeblendet
% - Titel des Fensters lautet 'Taschenrechner' statt 'Figure N'
% - Taschenrechner ist von der Tastatur aus bedienbar
% - Syntaktisch falsche Eingaben (z.B. nicht geschlossene Klammer) f???hren
%   zu keinem Fehler, sondern geben eine Warnung auf der Konsole aus; der
%   Ausdruck kann in diesem Fall durch DEL berichtigt werden [try-catch]
%
% Hinweise
% - GUI-Tasten sind als [uicontrol('Style', 'pushbutton',...)]
%   implementiert
% - GUI-Anzeigen sind als [uicontrol('Style', 'edit',...)] implementiert
%   und inaktiv geschaltet (keine Texteingabe m???glich) [Property: 'enable']
% - Benutzen Sie KEINE globalen Variablen! Verwenden Sie stattdessen z.B.
%   [guidata], um Variablen f???r verschiedene Callbacks sichtbar zu machen.
% - Tastaturbelegung: "=" -> return, "DEL" -> backspace, "CLR" -> escape
% - Sie k???nnen die Eingabe mehrerer Operationen z.B. durch Generierung und
%   Evaluierung von Befehls-Strings [eval] umsetzen
% - Wie Sie sehen, ist die GUI vollst???ndig in der p-Datei(kodierte m-Datei) 
%   enthalten. Im Gegensatz dazu bestehen GUI's, die mit Hilfe von GUIDE 
%   erstellt wurden, aus *.fig und *.m-Dateien. Ihnen ist es freigestellt,
%   diese Aufgabe mit der Methode Ihrer Wahl zu implemenieren. Falls Sie 
%   sich f???r die programmatische Umsetzung entscheiden, finden Sie 
%   Hilfestellung dazu unter 
%   http://www.mathworks.de/help/matlab/creating_guis/about-the-simple-programmatic-gui-example.html


%% L??sung
close all;
clear all;
clc;

%% Aufgabe
display('Aufgabe')
display('Loesung in der Datei calculator.m')
display('Taschenrechner gestartet.')
calculator