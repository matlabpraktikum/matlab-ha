% Aufg_WS2014_04_02_Mex_Performance.m
% Wintersemester 2014/2015
% Autor: Tim Habigt (tim@tum.de)
%        H. Abdellatif

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Schnittstellen: Effizienz-Analyse mit Mex-Funktion
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diese Aufgabe wurde bearbeitet von:
% Name: Markus Hofbauer
% Matrikelnummer: 03623454
%
% Name: Kevin Meyer
% Matrikelnummer: 03623271
%
% Name: Thomas Markowitz
% Matrikelnummer: 03623504
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemeine Hinweise: 
% Die Aufgaben koennen in Gruppen von bis zu drei Studierenden bearbeitet
% werden. Tragen Sie dazu bitte in alle Dateien, die Sie bearbeiten, die
% Namen und Matrikelnummern der Gruppenmitglieder ein. Pro Gruppe muss die
% Hausaufgabe nur einmal abgegeben werden. Werden mehrere Loesungen
% abgegeben, wird die neueste Abgabe bewertet.
% 
% Bitte bearbeiten Sie die Aufgaben direkt in den *.m-Dateien der Angabe.
% Bitte aendern Sie den Dateinamen nicht, um die Zuordnung zu erleichtern.
% Verpacken Sie die bearbeiteten Aufgaben bitte wieder als zip-Archiv. In
% diesem Archiv muessen alle Dateien enthalten sein, die zur Ausfuehrung
% der Skripte noetig sind (keine Autosave *.asv oder *.m~ Dateien).
% 
% Die Loesungen muessen bis zur jeweils angegebenen Deadline in Moodle
% (https://www.moodle.tum.de) hochgeladen werden. Wenn es ein Problem
% bei der Abgabe der Hausaufgaben gab, werden Sie per E-Mail
% benachrichtigt.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Beispiel zum Vergleich der Recheneffizienz
% Matrix Multiplikation
n = 100; A = rand(n , n) ; B = rand(n , n) ; C = zeros (n , n) ;
% Erste Methode: selbstprogrammierter Algorithmus mit for-Schleife
tic;
for ii = 1 : n
    for jj = 1 : n
        for kk = 1 : n
            C( ii , jj ) = C( ii , jj ) + A( ii , kk ) * B( kk , jj ) ;
        end
    end
end
timeLoop = toc;
% Zweite Methode: Multiplikation von Matlab verwenden
D = zeros (n , n) ;
tic ;
D = A*B;
timeMatrix = toc;

% Ergaenzen Sie das Beispiel, indem Sie die Alternative ueber eine
% Mex-Funktion untersuchen. 
% 1) Erstellen Sie eine Mex-Funtion aus C-code, welche eine Multiplikation 
% zweier quadratischer Matrizen der gleichen Dimensionen durchfuehrt.
% Sie koennen fuer die Multiplikation fremde Algorithmen verwenden, die Sie
% z.B. im Netz finden. 
% Stellen Sie sicher, dass im C-Code eine Plausibilitaetsueberpruefung der 
% uebergebenen Matrizen stattfindet (A und B sind quadratisch und dim (A) =
% dim (B))

% 2) Vergleichen Sie die Effizienz der drei Methoden grafisch und
% exportieren Sie den grafischen Vergleich in einer png-Datei.

%% L??sung
close all;
clc;

%% Aufgabe 1
display('Aufgabe 1')

display('Kompilieren von matrixMultiplicationMex.c')
mex matrixMultiplicationMex.c

tic;
E = matrixMultiplicationMex(A,B);
timeMex = toc;

disp(' ')
display(['Zeit Matlab for-loop:             ', num2str(timeLoop), ' Sekunden.'])
display(['Zeit Matlab Matrixmultiplikation: ', num2str(timeMatrix), ' Sekunden.'])
display(['Zeit Mex C-Code for-loop:         ', num2str(timeMex), ' Sekunden.'])
disp(' ')

%% Aufgabe 2
display('Aufgabe 2')
display('Die Perfomance des C-Codes ist besser als die des Matlab Codes mit Schleifen. Sie unterliegt aber der Matrixmultiplikation von Matlab.')

fig = figure;
axe = axes('Parent',fig,...
    'XTickLabel',{'Matlab loop','Matlab Matrix','Mex C loop'},...
    'XTick',[1 2 3]);
box(axe,'on');
hold(axe,'on');
ylabel('t [s]');
xlabel('Methode');
title('Effezienzvergleich Matrixmultiplikation');
bar([timeLoop, timeMatrix, timeMex]);

saveas(fig, 'mex_performance.png','png');
