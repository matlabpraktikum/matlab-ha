% Aufg_WS2014_04_04_Performance_Audio.m
% Wintersemester 2014/2015
% Autor: Tim Habigt (tim@tum.de)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Aufgabe 4.4: Performanceoptimierung
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diese Aufgabe wurde bearbeitet von:
% Name: Markus Hofbauer
% Matrikelnummer: 03623454
%
% Name: Kevin Meyer
% Matrikelnummer: 03623271
%
% Name: Thomas Markowitz
% Matrikelnummer: 03623504
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemeine Hinweise: 
% Die Aufgaben koennen in Gruppen von bis zu drei Studierenden bearbeitet
% werden. Tragen Sie dazu bitte in alle Dateien, die Sie bearbeiten, die
% Namen und Matrikelnummern der Gruppenmitglieder ein. Pro Gruppe muss die
% Hausaufgabe nur einmal abgegeben werden. Werden mehrere Loesungen
% abgegeben, wird die neueste Abgabe bewertet.
% 
% Bitte bearbeiten Sie die Aufgaben direkt in den *.m-Dateien der Angabe.
% Bitte aendern Sie den Dateinamen nicht, um die Zuordnung zu erleichtern.
% Verpacken Sie die bearbeiteten Aufgaben bitte wieder als zip-Archiv. In
% diesem Archiv muessen alle Dateien enthalten sein, die zur Ausfuehrung
% der Skripte noetig sind (keine Autosave *.asv oder *.m~ Dateien).
% 
% Die Loesungen muessen bis zur jeweils angegebenen Deadline in Moodle
% (https://www.moodle.tum.de) hochgeladen werden. Wenn es ein Problem
% bei der Abgabe der Hausaufgaben gab, werden Sie per E-Mail
% benachrichtigt.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;

% In der digitalen Signalverarbeitung muss in vielen Faellen das Signal vor
% der Verarbeitung "gefenstert" werden. Darunter versteht man die
% Aufteilung eines langen Signals in mehrere, gleich lange Stuecke
% (Frames), die jeweils mit einer Fensterfunktion multipliziert werden.
% Informationen dazu unter: http://de.wikipedia.org/wiki/Fensterfunktion

% Das folgende Beispiel zeigt die Anwendung eines Hanning-Fensters auf ein
% zufaelliges Signal:

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Beginn des Beispiels
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Laenge eines Frames
frameSize = 128;

% Fensterfunktion
win = hann(frameSize);

% Beispiel zur Fensterung
signal = 0.2*randn(frameSize,1);
figure, plot(signal);
hold on;
plot(win, 'k');
plot(signal.*win, 'r');
legend('Signal', 'Fensterfunktion', 'Ergebnisframe');
hold off;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ende des Beispiels
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Im Folgenden wird das Fenster auf eine laengere Audio-Sequenz angewendet.
% Das Ergebnis der Fensterung ist eine Matrix, die mehrere Frames
% beinhaltet. Dabei stellt die erste Spalte der Matrix die ersten 128
% Samples des Audiosignals gewichtet mit der Fensterfunktion dar. Die
% zweite Spalte beinhaltet die naechsten 128 gewichtetet Samples usw.
%
% Ziel der Aufgabe ist es, den Code, der sich innerhalb der Zeitmessung
% befindet, zu beschleunigen. Um halbwegs verlaessliche Werte fuer den
% Beschleunigungsfaktor zu erhalten, muessen Sie das Skript mehrmals
% ausfuehren.

% Signal einlesen
load('handel.mat');

% Verlaengere Signal auf ein ganzahliges Vielfaches der Framelaenge
y = [y; zeros(ceil(size(y,1)/frameSize)*frameSize - size(y,1),1)];

% Zaehle Anzahl der Frames
sigLen = size(y,1);
numFrames = sigLen / frameSize;

% Initialisiere Ergebnismatrizen
res = zeros(frameSize, numFrames);
res2 = zeros(frameSize, numFrames);
frame = zeros(frameSize,1);

% Start der Zeitmessung
tic;
cnt = 1;

for k = 1:frameSize:sigLen
    % Kopiere erstes Signalstueck aus dem Signal
    sigPart = y(k:k+frameSize-1);
    
    % Multipliziere Signalstueck mit dem Fenster
    for l = 1:frameSize
        frame(l,1) = sigPart(l,1) * win(l,1);
    end
    
    % Kopiere Frame in die Ergebnismatrix
    res(:,cnt) = frame;
    cnt = cnt + 1;
end

% Ende der Zeitmessung
disp('Laufzeit'); laufzeitOrg = toc

tic;
filter = win(:,ones(1,numFrames));
res2 = reshape(y,frameSize,[]).*filter;
laufzeitNeu = toc

Beschleunigungsfaktor = laufzeitOrg/laufzeitNeu

assert(isequal(res,res2))
display('Mittelwert Beschleunigungsfaktor: 23.4572')