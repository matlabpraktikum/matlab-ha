% Aufg_WS2014_02_01_eval.m
% Wintersemester 2014/2015
% Autor: Vladislav Okunev (v.okunev@tum.de)
%        T. Habigt
%        H. Abdellatif

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Einf???hrung II, Aufgabe 1: Umgang mit eval, dynamische Feldadressierung
% in Structure Arrays
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diese Aufgabe wurde bearbeitet von:
% Name: Markus Hofbauer
% Matrikelnummer: 03623454
%
% Name: Kevin Meyer
% Matrikelnummer: 03623271
%
% Name: Thomas Markowitz
% Matrikelnummer: 03623504
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemeine Hinweise: 
% Die Aufgaben koennen in Gruppen von bis zu drei Studierenden bearbeitet
% werden. Tragen Sie dazu bitte in alle Dateien, die Sie bearbeiten, die
% Namen und Matrikelnummern der Gruppenmitglieder ein. Pro Gruppe muss die
% Hausaufgabe nur einmal abgegeben werden. Werden mehrere Loesungen
% abgegeben, wird die neueste Abgabe bewertet.
% 
% Bitte bearbeiten Sie die Aufgaben direkt in den *.m-Dateien der Angabe.
% Bitte aendern Sie den Dateinamen nicht, um die Zuordnung zu erleichtern.
% Verpacken Sie die bearbeiteten Aufgaben bitte wieder als zip-Archiv. In
% diesem Archiv muessen alle Dateien enthalten sein, die zur Ausfuehrung
% der Skripte noetig sind (keine Autosave *.asv oder *.m~ Dateien).
% 
% Die Loesungen muessen bis zur jeweils angegebenen Deadline in Moodle
% (https://www.moodle.tum.de) hochgeladen werden. Wenn es ein Problem
% bei der Abgabe der Hausaufgaben gab, werden Sie per E-Mail
% benachrichtigt.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% 1) Im Ordner 'Aufg_WS2014_02_01_data' befinden sich mehrere Datens???tze. 
% Speichern Sie die Namen dieser *.mat-Dateien mit Hilfe von [dir] in einem 
% Cell Array. (NUR *.mat-Dateien)

% Jede Datei enth???lt ein Signal 'signal' und mehrere unterschiedliche 
% Zeitvektoren. Der Name des zu dem Signal passenden Zeitvektors ist in der
% Variablen 'signal_t' gespeichert.

% 2) Iterieren Sie ???ber die Elemente des in (1) erzeugten Cell Arrays 
% und laden Sie die Variablen aus der jeweiligen Datei in das Workspace.
% (NICHT in eine Struct).
% Bestimmen Sie die Abtstrate des Signals "signal" in jeder Datei aus dem
% entsprechenden Zeitvektor. Lesen Sie hierf???r jeweils den aktuellen 
% Zeitvektor, dessen Name in 'signal_t' gegeben ist, mit Hilfe von [eval]
% in die Variable 'Zeitvektor' ein. Geben Sie die Abtastrate auf der 
% Konsole aus.


% 3) Es ist m???glich, diese Aufgabe ohne [eval] zu l???sen. Laden Sie hierzu
% die Dateien wieder in einer Schleife, diesmal jedoch jeweils in eine 
% Struct [myStruct = load()]. Benutzen Sie dann sogenannte dynamic field 
% names (Dokumentation/Google: "Generate Field Names from Variables"), um 
% den jeweiligen Zeitvektor in die Variable 'Zeitvektor' zu laden. Geben 
% Sie die Abtastrate wieder auf der Konsole aus.


% 4) Jetzt sollen alle Signale samt dazugeh???riger Zeitvektoren in einer
% Datei gespeichert werden. Iterieren Sie hierzu wieder durch alle Dateien,
% laden Sie jedes Signal in die Variable 'Signal_N' und den dazugeh???rigen
% Zeitvektor in die Variable 'Zeitvektor_N', wobei N der laufenden Nummer 
% der Datei entspricht [eval]. Speichern Sie anschlie???end die erzeugten 
% Signal- und Zeitvektorvariablen als Vektoren in der Datei 'container.mat' 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% L??sung
close all;
clear all;
clc;

%% Aufgabe 1
display('Aufgabe 1')
files = dir('Aufg_WS2014_02_01_data/*.mat');
cells = cell(size(files));
for k=1:size(files,1)
    cells{k} = files(k).name;
end
cells
disp(' ')

%% Aufgabe 2
display('Aufgabe 2')
for k=1:size(cells,1)
    load(strcat('Aufg_WS2014_02_01_data/',cells{k}));
    Zeitvektor = eval(signal_t);
    sample = (length(Zeitvektor) - 1) / (Zeitvektor(end) - Zeitvektor(1));
    display(['Abtastrate von Array ', num2str(k), ': ', num2str(sample), ' Hz'])
end
disp(' ')

%% Aufgabe 3
display('Aufgabe 3')
for k=1:size(cells,1)
    mystruct = load(strcat('Aufg_WS2014_02_01_data/', cells{k}));
    Zeitvektor = mystruct.(mystruct.signal_t);
    sample = (length(Zeitvektor) - 1) / (Zeitvektor(end) - Zeitvektor(1));
    display(['Abtastrate von Array ', num2str(k), ': ', num2str(sample), ' Hz'])
end
disp(' ')

%% Aufgabe 4
display('Aufgabe 4')
for k=1:size(cells,1)
    mystruct = load(strcat('Aufg_WS2014_02_01_data/', cells{k}));
    eval(['Signal_', num2str(k), '= mystruct.signal;']);
    eval(['Zeitvektor_', num2str(k), '= mystruct.(mystruct.signal_t);']);
end
save('container.mat', 'Signal_*', 'Zeitvektor_*')
display('Datei gespeichert.')
disp(' ')
