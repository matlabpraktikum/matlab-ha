% Aufg_WS2014_01_01_Speicherplatzbedarf.m
% Wintersemester 2014/2015
% Autor: Tim Habigt (tim@tum.de)
%        H. Abdellatif

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Einfuehrung I, Aufgabe 1: Speicherplatzbedarf
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diese Aufgabe wurde bearbeitet von:
% Name: Markus Hofbauer
% Matrikelnummer: 03623454
%
% Name: Kevin Meyer
% Matrikelnummer: 03623271
%
% Name: Thomas Markowitz
% Matrikelnummer: 03623504
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemeine Hinweise: 
% Die Aufgaben koennen in Gruppen von bis zu drei Studierenden bearbeitet
% werden. Tragen Sie dazu bitte in alle Dateien, die Sie bearbeiten, die
% Namen und Matrikelnummern der Gruppenmitglieder ein. Pro Gruppe muss die
% Hausaufgabe nur einmal abgegeben werden. Werden mehrere Loesungen
% abgegeben, wird die neueste Abgabe bewertet.
% 
% Bitte bearbeiten Sie die Aufgaben direkt in den *.m-Dateien der Angabe.
% Bitte aendern Sie den Dateinamen nicht, um die Zuordnung zu erleichtern.
% Verpacken Sie die bearbeiteten Aufgaben bitte wieder als zip-Archiv. In
% diesem Archiv muessen alle Dateien enthalten sein, die zur Ausfuehrung
% der Skripte noetig sind (keine Autosave *.asv oder *.m~ Dateien).
% 
% Die Loesungen muessen bis zur jeweils angegebenen Deadline in Moodle
% (https://www.moodle.tum.de) hochgeladen werden. Wenn es ein Problem
% bei der Abgabe der Hausaufgaben gab, werden Sie per E-Mail
% benachrichtigt.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 1) Erzeugen Sie einen Spaltenvektor mit 12 Millionen zufaelligen
% Eintraegen. Wieviel Arbeitsspeicher benoetigt MATLAB vor und nach dem
% Anlegen der Variablen? (Windows: Task-Manager, Linux: z. B. top)  Wie
% viele Bytes belegt demnach ein Element des Vektors?

% 2) Weisen Sie den Vektor einer weiteren Variablen zu.  Wieviel
% Speicherplatz benoetigt Matlab nun?  Was schliessen Sie daraus?

% 3) Weisen Sie die mit (:) linearisierte Variable einer weiteren Variablen
% zu.  Was schliessen Sie aus dem neuen Speicherplatzbedarf von Matlab?

% 4) Veraendern Sie das letzte Element der zuletzt erzeugten Variablen.
% Was schliessen Sie aus dem neuen Speicherplatzbedarf von Matlab?

% 5) Weisen Sie die Transponierte der ersten Variable einer weiteren
% Variablen zu.  Was schliessen Sie aus dem neuen Speicherplatzbedarf von
% Matlab?

% 6) Loeschen Sie den Matlab Workspace [clear], legen Sie eine eine
% Zufallsmatrix mit 12 Millionen Zeilen und zwei Spalten an und notieren
% Sie dann den Speicherplatzbedarf von Matlab.

% 7) Weisen Sie die mit (:) linearisierte Variable aus 6) einer neuen
% Variablen zu.  Was schliessen Sie aus dem neuen Speicherplatzbedarf von
% Matlab?

% 8) Weisen Sie die Transponierte der Variable aus 6) einer neuen Variablen
% zu.  Was schliessen Sie aus dem neuen Speicherplatzbedarf von Matlab?

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% L??sung
close all;
clear all;
clc;

%% Aufgabe 1
display('Aufgabe 1')
vec = rand(12e6,1);
display('Davor  : 3543900 kB')
display('Danach : 3637652 kB')
bytes = (3637652 - 3543900) * 1024 / 12e6;
display(['Belegte Bytes pro Element: ', num2str(bytes)])
disp(' ')

%% Aufgabe 2
display('Aufgabe 2')
vec2 = vec;
display('Es wird kein weiterer Speicherplatz ben??tigt. Matlab verwendet ein intelligentes Speichersystem und speichert den Inhalt der neuen Variable nicht nochmals.')
disp(' ')

%% Aufgabe 3
display('Aufgabe 3')
vec_lin = vec2(:);
display('Es wird ebenfalls kein weiter Speicher verwendet. Matlab speichert den Inhalt der neuen Variable nicht nochmals.')
disp(' ')

%% Aufgabe 4
display('Aufgabe 4')
vec_lin(end) = 1;
display('Es werden ca. 100MB (wie beim ersten Speichervorgang) neuer Speicher belegt. Das bedeutet Matlab speichert nur den Vektor erneut ab.')
disp(' ')

%% Aufgabe 5
display('Aufgabe 5')
vec_t = vec';
display('Es wird ebenfalls kein weiter Speicher verwendet. Matlab speichert den Inhalt der neuen Variable nicht nochmals.')
disp(' ')

%% Aufgabe 6
clear
display('Aufgabe 6')
mat = rand(12e6,2);
display('Verwendeter Speicherplatzbedarf: ca. 200MB')
disp(' ')

%% Aufgabe 7
display('Aufgabe 7')
mat_lin = mat(:);
display('Es wird ebenfalls kein weiter Speicher verwendet. Matlab speichert den Inhalt der neuen Variable nicht nochmals.')
disp(' ')

%% Aufgabe 8
display('Aufgabe 8')
mat_t = mat';
display('Im Gegensatz zur Transponierung des Vektors wird hier die Variable neu abgespeichert, da sich die Indices der Eintr?ge ?ndern.')