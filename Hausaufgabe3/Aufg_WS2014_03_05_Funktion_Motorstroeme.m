% Aufg_SS2014_03_05_function_motorstroeme.m
% Wintersemester 2014/2015
% Author: H. Abdellatif (houssem.abdellatif@ldv.ei.tum.de)
%                       (houssem.abdellatif@itk-engineering.de)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Programmiertechnik I, Aufgabe 2: Funktionsentwicklung
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diese Aufgabe wurde bearbeitet von:
% Name: Markus Hofbauer
% Matrikelnummer: 03623454
%
% Name: Kevin Meyer
% Matrikelnummer: 03623271
%
% Name: Thomas Markowitz
% Matrikelnummer: 03623504
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemeine Hinweise: 
% Die Aufgaben koennen in Gruppen von bis zu drei Studierenden bearbeitet
% werden. Tragen Sie dazu bitte in alle Dateien, die Sie bearbeiten, die
% Namen und Matrikelnummern der Gruppenmitglieder ein. Pro Gruppe muss die
% Hausaufgabe nur einmal abgegeben werden. Werden mehrere Loesungen
% abgegeben, wird die neueste Abgabe bewertet.
% 
% Bitte bearbeiten Sie die Aufgaben direkt in den *.m-Dateien der Angabe.
% Bitte aendern Sie den Dateinamen nicht, um die Zuordnung zu erleichtern.
% Verpacken Sie die bearbeiteten Aufgaben bitte wieder als zip-Archiv. In
% diesem Archiv muessen alle Dateien enthalten sein, die zur Ausfuehrung
% der Skripte noetig sind (keine Autosave *.asv oder *.m~ Dateien).
% 
% Die Loesungen muessen bis zur jeweils angegebenen Deadline in Moodle
% (https://www.moodle.tum.de) hochgeladen werden. Wenn es ein Problem
% bei der Abgabe der Hausaufgaben gab, werden Sie per E-Mail
% benachrichtigt.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Es soll eine Funktion entwickelt werden, die aus den gemessenen Momenten
% zweier parallel geschalteter Elektromaschinen den jeweiligen Strom
% berechnet
% [i1, i2] = motorstroeme (M1, M2, I)
% wobei I der gemessene Gesamtstrom I ist. Die Berechnungsformeln lauten
% i1 = I / (1+(M2/M1))
% i2 = I / (1+(M1/M2))

% Schreiben Sie diese Funktion mit folgenden Randbedingungen:
% - Die Funktion akzeptiert nur skalare Groessen (keine Matrizen, Vektoren
%   oder Empty-Werte) sonst wird diese mit einer Warnung sofort beendet. 
%   ([warning], nicht [disp])
% - Die Funktion akzeptiert exakt drei Eingangsargumente. Wird diese Anzahl
%   unterschritten bzw. ueberschritten, wird die Funktion sofort mit einer
%   Warnung beendet.
% - Die Funktion darf mit hoechstens zwei Ausgangsargumenten aufgerufen
%   werden. Wird diese Anzahl ueberschritten so soll eine Warnung
%   ausgegeben aber die Funktion zu Ende ausgefuehrt werden. Wird die
%   Anzahl unterschritten so werden die fehlenden Ausgangsargumente von der
%   Funktion ausgegeben (disp).
% - Achten Sie darauf den Sonderfall M1=0 und M2=0 zu beruecksichtigen.

% Hinweise:
% Folgende Werte sind fuer Tests realistisch: M1=200, M2=300; I=20;

%% L??sung
close all;
clear all;
clc;

%% Aufgabe
display('Aufgabe')
display('Loesung in der Datei motorstroeme.m')
disp(' ')
display('Korrekter Aufruf ohne Rueckgabevektor:')
motorstroeme(200,300,20);
display('Korrekter Aufruf mit i1 als Rueckgabevektor:')
i1 = motorstroeme(200,300,20)
display('Korrekter Aufruf mit [i1, i2] als Rueckgabevektor:')
[i1, i2] = motorstroeme(200,300,20)

display('Aufruf mit mehr als 2 Rueckgabeargumenten:')
[i1, i2, i3] = motorstroeme(200,300,20)
display('Aufruf mit mehr als 3 Eingabeargumenten:')
motorstroeme(200,300,20,20);
display('Aufruf mit weniger als 3 Eingabeargumenten:')
motorstroeme(200,300);
display('Aufruf mit Matrix und empty als Eingabeargumente:')
motorstroeme([200,1],300,[]);
display('Wir ueberpruefen zusaetzlich ob es sich bei dem skalaren Eingabeargument um eine Zahl handelt.')
disp(' ')
display('Aufruf mit Sonderfall M1 = 0 und M2 = 0:')
motorstroeme(0,0,3);

clear