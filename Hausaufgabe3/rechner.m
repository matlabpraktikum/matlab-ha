function [ varargout ] = rechner( array1, array2, calcTypeString )

switch calcTypeString
    case 'addition'
        result = array1 + array2;
    case 'subtraktion'
        result = array1 - array2;
    case 'multiplikation'
        result = array1 .* array2;
    case 'division'
        result = array1 ./ array2;
end

varargout{1} = result;
if nargout == 0
    disp(result)
end

end

