% Aufg_WS2014_01_03_Operatoren.m
% Wintersemester 2014/2015
% Autor: Vladislav Okunev (v.okunev@tum.de)
%        T. Habigt
%        H. Abdellatif

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Einfuehrung I, Aufgabe 3: Short-Circuit-Operatoren, Operatorenrangfolge
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diese Aufgabe wurde bearbeitet von:
% Name: Markus Hofbauer
% Matrikelnummer: 03623454
%
% Name: Kevin Meyer
% Matrikelnummer: 03623271
%
% Name: Thomas Markowitz
% Matrikelnummer: 03623504
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemeine Hinweise:
% Die Aufgaben koennen in Gruppen von bis zu drei Studierenden bearbeitet
% werden. Tragen Sie dazu bitte in alle Dateien, die Sie bearbeiten, die
% Namen und Matrikelnummern der Gruppenmitglieder ein. Pro Gruppe muss die
% Hausaufgabe nur einmal abgegeben werden. Werden mehrere Loesungen
% abgegeben, wird die neueste Abgabe bewertet.
%
% Bitte bearbeiten Sie die Aufgaben direkt in den *.m-Dateien der Angabe.
% Bitte aendern Sie den Dateinamen nicht, um die Zuordnung zu erleichtern.
% Verpacken Sie die bearbeiteten Aufgaben bitte wieder als zip-Archiv. In
% diesem Archiv muessen alle Dateien enthalten sein, die zur Ausfuehrung
% der Skripte noetig sind (keine Autosave *.asv oder *.m~ Dateien).
%
% Die Loesungen muessen bis zur jeweils angegebenen Deadline in Moodle
% (https://www.moodle.tum.de) hochgeladen werden. Wenn es ein Problem
% bei der Abgabe der Hausaufgaben gab, werden Sie per E-Mail
% benachrichtigt.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 1) Informieren Sie sich ???ber Short-Circuit-Operatoren [||] und [&&].
% Wodurch unterschieden sich diese von den elementweisen Operatoren
% [|] und [&]?

% 2) Betrachten Sie die untenstehenden Beispiele, erkl???ren Sie die
% Unterschiede und insbesondere die jeweiligen Fehlermeldungen. Wann sind
% die Short-Circuit- und elementweise Operatoren austauschbar?
a = [1 2 3];
b = [4 5 6];
result1 = isequal( size(a), size(b) ) &  sqrt( a.^2 + b.^2 ) < 5;
%result2 = isequal( size(a), size(b) ) && sqrt( a.^2 + b.^2 ) < 5;

b = [4 5];
%result3 = isequal( size(a), size(b) ) &  sqrt( a.^2 + b.^2 ) < 5;
result4 = isequal( size(a), size(b) ) && sqrt( a.^2 + b.^2 ) < 5;


% 3) Informieren Sie sich ???ber die Operatorenrangfolge in Matlab
% (Dokumentation: Operator Precedence). Zeigen Sie, in welcher Reihenfolge
% die Operationen in untenstehenden Beispielen ausgef???hrt werden, indem Sie
% Klammern an geeigneten Stellen setzen. Achten Sie auf Vollst???ndigkeit der
% Darstellung.
%   A = 10/7*2/4
%   B = [1 2 3].^2' + [3 2 1].^2'
%   C = 2^3^4
%   D = 3/10^3*7
%   E = 7-2^2
%   F = 4e2-1+2e-1+1


% 4) F???hren Sie die untenstehenden Zeilen aus. Sind a und b exakt gleich?
% H???ngt dies mit der Operatorrangfolge zusammen?
a = 1 - 2 + 2e-13;
b = 1 + 2e-13 - 2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% L??sung
close all;
clear all;
clc;
%% Aufgabe 1
display('Aufgabe 1')
display('Short-Circuit-Operatoren sind wie Logik Gatter zu betrachten. Sie k?nnen nur mit Logischen Signalen arbeiten (Matlab: 1-dim-Zahlenwerte: 0 = false, sonst true).')
display('Die elementweise Operatoren verodern bzw. verunden einzelne Werte aus Vektoren bzw. Matrizen. Dabei werden die einzelnen Eintr?ge mit selber Logik wie bei den Short-Circuit-Operatoren ausgewertet.')
disp(' ')

%% Aufgabe 2
display('Aufgabe 2')
display('result2: Fehlermeldung weist darauf hin, dass && nur mit logischen Signalen umgehen kann.')
display('Fall: 1 && [1,0,0] ist nicht m?glich, da der Vektor kein logisches Signal ist. Bei 1 & [1,0,0] wird der Vektor elementweise mit 1 verundet, welches in [1,0,0] resultiert.')
disp(' ')

display('result3: Matlab (und kein Mathematiker) k?nnen Vektoren unterschiedlicher Dimension addieren.')
display('Jedoch wirft result4 keinen Fehler, da der &&-Operator abbricht, sobald ein Vergleichswert der &&-Operation false ist. Die Fehlerproduzierende zweite Operation wird in diesem Fall also nicht ausgewertet, da isequal() false zur?ckgibt.')
disp(' ')

%% Aufgabe 3
display('Aufgabe 3')
display('A = (((10/7)*2)/4)')
display('B = ((([1 2 3].^2)'') + (([3 2 1].^2)''))')
display('C = ((2^3)^4)')
display('D = ((3/(10^3))*7)')
display('E = (7-(2^2))')
display('F = ((((4e2)-1)+(2e(-1)))+1)')
disp(' ')

%% Aufgabe 4
display('Aufgabe 4')
display('Das Ausf??hren der der Zeilen')
display('    a = 1 - 2 + 2e-13;')
display('    b = 1 + 2e-13 - 2;')
a = 1 - 2 + 2e-13;
b = 1 + 2e-13 - 2;
display(['liefert f??r a == b: ', num2str(a == b)])
display('Aufgrund der Maschinen(un)genauigkeit f?hrt eine Ver?nderung der Reihenfolge in diesem Fall zu nicht exakt gleichen Ergebnissen.')
