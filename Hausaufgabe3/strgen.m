function [ string ] = strgen( varargin )

string = strjoin(cellfun(@num2str, varargin, 'UniformOutput', false));

end

