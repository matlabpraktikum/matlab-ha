# Projektpraktikum Matlab

## Matlab Hausaufgaben, Gruppe 10, WS 2014/2015

This is a project of students of *Electrical Engineering and Information Technology* at [Technical University of Munich](http://www.tum.de) (TUM).  
The Homework is part of [LDV's](http://www.ldv.ei.tum.de) course [Projektpraktikum Matlab](http://www.ldv.ei.tum.de/lehre/projektpraktikum-matlab/).

**Written by:**

-   Markus Hofbauer
-   Kevin Meyer
-   Thomas Markowitz

Check out our main project at [othello](https://bitbucket.org/matlabpraktikum/othello)