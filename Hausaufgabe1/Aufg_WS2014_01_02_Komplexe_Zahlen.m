% Aufg_WS2014_01_02_Komplexe_Zahlen.m
% Wintersemester 2014/2015
% Autor: Vladislav Okunev (v.okunev@tum.de)
%        T. Habigt
%        H. Abdellatif

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Einf???hrung I, Aufgabe 2: Komplexe Zahlen, Speicherplatzbedarf
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diese Aufgabe wurde bearbeitet von:
% Name: Markus Hofbauer
% Matrikelnummer: 03623454
%
% Name: Kevin Meyer
% Matrikelnummer: 03623271
%
% Name: Thomas Markowitz
% Matrikelnummer: 03623504
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemeine Hinweise:
% Die Aufgaben koennen in Gruppen von bis zu drei Studierenden bearbeitet
% werden. Tragen Sie dazu bitte in alle Dateien, die Sie bearbeiten, die
% Namen und Matrikelnummern der Gruppenmitglieder ein. Pro Gruppe muss die
% Hausaufgabe nur einmal abgegeben werden. Werden mehrere Loesungen
% abgegeben, wird die neueste Abgabe bewertet.
%
% Bitte bearbeiten Sie die Aufgaben direkt in den *.m-Dateien der Angabe.
% Bitte aendern Sie den Dateinamen nicht, um die Zuordnung zu erleichtern.
% Verpacken Sie die bearbeiteten Aufgaben bitte wieder als zip-Archiv. In
% diesem Archiv muessen alle Dateien enthalten sein, die zur Ausfuehrung
% der Skripte noetig sind (keine Autosave *.asv oder *.m~ Dateien).
%
% Die Loesungen muessen bis zur jeweils angegebenen Deadline in Moodle
% (https://www.moodle.tum.de) hochgeladen werden. Wenn es ein Problem
% bei der Abgabe der Hausaufgaben gab, werden Sie per E-Mail
% benachrichtigt.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 1) Erzeugen Sie einen zuf???lligen Zeilenvektor mit 3 komplexen Zahlen.

% 2) Transponieren Sie den komplexwertigen Vektor a) mit dem [']-Operator
% und b) mit dem [(:)]-Operator. Vergleichen Sie die Ergebnisse. Erkl???ren
% Sie den Unterschied. Welche Funktion (m-Datei) ruft ['] auf (Doku)?

% 3) Welche Funktion liefert die nicht-konjugierte Transponierte?
% Transponieren Sie den Vektor aus 2b), um wieder den unspr???nglichen
% Vektor zu erhalten.

% 4) Erzeugen Sie zwei reelle Zufallvektoren mit je 7 Millionen Eintr???gen.
% Wie viel Arbeitsspeicher belegt Matlab vor und nach der Erzeugung? Wie
% viele Bytes werden demnach von jedem Eintrag belegt?

% 5) Interpretieren Sie die erzeugten Vektoren als Real- und Imagin???rteile
% und kombinieren Sie sie zu einem komplexwertigen Vektor. Wie ???ndert sich
% der Speicherplatzbedarf? Wie viele Bytes belegt ein Element des komplex-
% wertigen Vektors?

% 6) Weisen Sie die mit [(:)] Transponierte dieses Vektors einer neuen
% Variablen zu. Wie ???ndert sich jeweils der Speicherplatzbedarf von Matlab?
% Begr???nden Sie das Verhalten.

% 7) Weisen Sie nun die konjugierte Transponierte des Vektors einer neuen
% Variablen zu. ???ndert sich jetzt der Speicherplatzbedarf? Wird neuer
% Speicherplatz f???r den Real- oder Imagin???rteil allokiert? Erkl???ren Sie
% den Unterschied zu 6).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% L??sung
close all;
clear all;
clc;

%% Aufgabe 1
display('Aufgabe 1')
vec = complex(rand(1,3),rand(1,3))
disp(' ')

%% Aufgabe 2
display('Aufgabe 2')
vec_t = vec'
vec_lin = vec(:)
display('[:] erzeugt aus dem Zeilenvektor vec einen Spaltenvektor.')
display('[''] transponiert den Zeilenvektor vec mit der Complex conjugate Transpose.')
display('Der Spaltenvektor enth??lt die konkugiert Komplexen Elemente von vec. Dabei wir die Funktion [ctranspose] aufgerufen.')
disp(' ')

%% Aufgabe 3
display('Aufgabe 3')
display('Die Funktion [transpose] oder [.''] liefert die nicht-konjugiert komplexe.')
vec_unlin = vec_lin.'
disp(' ')

%% Aufgabe 4
display('Aufgabe 4')
real1 = rand(1,7e6);
real2 = rand(1,7e6);
display('Davor  : 3542912 kB')
display('Danach : 3652288 kB')
bytes = (3652288 - 3542912) * 1024 / (2*7e6);
display(['Belegte Bytes pro Element: ', num2str(bytes)])
disp(' ')

%% Aufgabe 5
display('Aufgabe 5')
comp = complex(real1, real2);
display('Das speichern der komplexwertigen Zahl ben??tig den gleichen Speicherbedarf wie das Speichern von Realteil und Imagin??rteil.')
bytes = (3761664 - 3652288) * 1024 / (2*7e6);
display(['Belegte Bytes pro Element bei complex: ', num2str(bytes)])
disp(' ')

%% Aufgabe 6
display('Aufgabe 6')
comp_t = comp(:);
display('Es wird kein weiterer Speicherplatz ben??tigt, da sich die Indizierung und die zugehörigen Werte der Komponenten im Vektor nicht ??ndern.')
disp(' ')

%% Aufgabe 7
display('Aufgabe 7')
comp_ct = comp';
display('Ja, der Speicherpltzbedarf ??ndert sich. Es wird der gleiche Speicherplatz ben??tig, wie beim Ausgangselement, d.h es werden Real- und Imagin??rteil neu gespeichert. Im Gegensatz zu 6) ??ndert sich hier jeweils der Wert.')
