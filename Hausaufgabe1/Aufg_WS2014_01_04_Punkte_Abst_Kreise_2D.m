% Aufg_WS2014_01_04_Punkte_Abst_Kreise_2D.m
% Wintersemester 2014/2015
% Autor: Tim Habigt (tim@tum.de)
%        H. Abdellatif

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Einfuehrung I, Aufgabe 4: Punkte, Abstaende und Kreise in 2D.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diese Aufgabe wurde bearbeitet von:
% Name: Markus Hofbauer
% Matrikelnummer: 03623454
%
% Name: Kevin Meyer
% Matrikelnummer: 03623271
%
% Name: Thomas Markowitz
% Matrikelnummer: 03623504
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemeine Hinweise: 
% Die Aufgaben koennen in Gruppen von bis zu drei Studierenden bearbeitet
% werden. Tragen Sie dazu bitte in alle Dateien, die Sie bearbeiten, die
% Namen und Matrikelnummern der Gruppenmitglieder ein. Pro Gruppe muss die
% Hausaufgabe nur einmal abgegeben werden. Werden mehrere Loesungen
% abgegeben, wird die neueste Abgabe bewertet.
% 
% Bitte bearbeiten Sie die Aufgaben direkt in den *.m-Dateien der Angabe.
% Bitte aendern Sie den Dateinamen nicht, um die Zuordnung zu erleichtern.
% Verpacken Sie die bearbeiteten Aufgaben bitte wieder als zip-Archiv. In
% diesem Archiv muessen alle Dateien enthalten sein, die zur Ausfuehrung
% der Skripte noetig sind (keine Autosave *.asv oder *.m~ Dateien).
% 
% Die Loesungen muessen bis zur jeweils angegebenen Deadline in Moodle
% (https://www.moodle.tum.de) hochgeladen werden. Wenn es ein Problem
% bei der Abgabe der Hausaufgaben gab, werden Sie per E-Mail
% benachrichtigt.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Hinweis: diese Aufgabe soll OHNE Schleifen programmiert werden!

% 1) Erzeugen Sie die Koordinaten von 10 Zufallspunkten in 2D [rand].

% 2) Stellen Sie die Punkte grafisch dar [scatter].

% 3) Berechnen Sie die Abstaende des letzten Punktes zu allen anderen
% Punkten.

% 4) Finden Sie den Punkt mit minimalem Abstand zum letzten Punkt [min] und
% zeichnen Sie eine Linie zwischen den beiden Punkten [line].

% 5) Berechnen Sie den Kreis um den letzten Punkt zentriert, der durch den
% in 4) bestimmten Punkt geht und zeichnen Sie diesen Kreis mittels der
% Linienfunktion [line].

%% HINWEIS
% Die Funktion "distance" darf in dieser Aufgabe NICHT verwendet werden, 
% da diese kein Bestandteil des Standard Matlab/Simulink-Packet ist,
% sondern eine Funktion der Neural Network Toolbox.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% L??sung
close all;
clear all;
clc;

figure('name', 'Aufgabe Abst Kreise')
hold on;
axis equal;
axis([0 1 0 1]);

%% Aufgabe 1
display('Aufgabe 1')
coord = rand(2,10)
disp(' ')

%% Aufgabe 2
display('Aufgabe 2')
display('Siehe Figure')
scatter(coord(1,:), coord(2,:));
disp(' ')

%% Aufgabe 3
display('Aufgabe 3')
x = coord(:,end);
dist = sqrt((x(1)-coord(1,1:end-1)).^2 + (x(2)-coord(2,1:end-1)).^2)
disp(' ')

%% Aufgabe 4
display('Aufgabe 4')
[dist_min,index_min] = min(dist);
point_min = coord(:,index_min);
line([point_min(1),x(1)],[point_min(2),x(2)]);
display(['Der n?chste Punkt ist der ', num2str(index_min), '. mit einem Abstand von ', num2str(dist_min)])
disp(' ')

%% Aufgabe 5
display('Aufgabe 5')
t = linspace(-2*pi,2*pi);
circ_x = x(1) + dist_min * sin(t);
circ_y = x(2) + dist_min * cos(t);

line(circ_x,circ_y);
display('Siehe Figure')
disp(' ')
