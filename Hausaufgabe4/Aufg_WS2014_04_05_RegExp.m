% Aufg_WS2014_04_05_RegExp.m
% Wintersemester 2014/2015
% Autor: Tim Habigt (tim@tum.de)
%        H. Abdellatif

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Aufgabe 4.5: Regular Expressions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diese Aufgabe wurde bearbeitet von:
% Name: Markus Hofbauer
% Matrikelnummer: 03623454
%
% Name: Kevin Meyer
% Matrikelnummer: 03623271
%
% Name: Thomas Markowitz
% Matrikelnummer: 03623504
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemeine Hinweise: 
% Die Aufgaben koennen in Gruppen von bis zu drei Studierenden bearbeitet
% werden. Tragen Sie dazu bitte in alle Dateien, die Sie bearbeiten, die
% Namen und Matrikelnummern der Gruppenmitglieder ein. Pro Gruppe muss die
% Hausaufgabe nur einmal abgegeben werden. Werden mehrere Loesungen
% abgegeben, wird die neueste Abgabe bewertet.
% 
% Bitte bearbeiten Sie die Aufgaben direkt in den *.m-Dateien der Angabe.
% Bitte aendern Sie den Dateinamen nicht, um die Zuordnung zu erleichtern.
% Verpacken Sie die bearbeiteten Aufgaben bitte wieder als zip-Archiv. In
% diesem Archiv muessen alle Dateien enthalten sein, die zur Ausfuehrung
% der Skripte noetig sind (keine Autosave *.asv oder *.m~ Dateien).
% 
% Die Loesungen muessen bis zur jeweils angegebenen Deadline in Moodle
% (https://www.moodle.tum.de) hochgeladen werden. Wenn es ein Problem
% bei der Abgabe der Hausaufgaben gab, werden Sie per E-Mail
% benachrichtigt.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Die Namen der 18 Teilnehmer eines Sprachkurses wurden in Matlab erfasst
% und unter der Variable "Namen" abgelegt. Jeder Name wird strikt als
% '<Vorname>_<Nachname>' abgelegt. Gross- bzw. Kleinschreibung spielt dabei
% keine Rolle.
%
% a) Verwenden Sie die "regular expressions", um alle Teilnehmer zu 
% erfassen, deren Nachname eine Variante von Meyer ist. Bei Doppelnamen
% werden die einzelnen Nachnamen ueberprueft.
% Die deutschen Varianten von Meyer sind die folgenden:
% Meier, Meyer, Maier, Mayer, Mayr oder Mair.

Namen {1}  = 'Max Mustermann';
Namen {2}  = 'Ulrike Maier';
Namen {3}  = 'Markus Schmidt';
Namen {4}  = 'Bram Mejer';
Namen {5}  = 'Jasmin Huber';
Namen {6}  = 'Sepp Mayr';
Namen {7}  = 'Sebastian Meyer';
Namen {8}  = 'Torsten Hohlstein';
Namen {9}  = 'Andrea Maier';
Namen {10} = 'Andreas Mueller';
Namen {11} = 'Jasper von Hausen';
Namen {12} = 'Silke Habenicht';
Namen {13} = 'Marius Mueller-Westernhagen';
Namen {14} = 'Maria Meyer';
Namen {15} = 'Thorben Mair';
Namen {16} = 'Antonia Stefanie Mayer';
Namen {17} = 'Lisa Stean';
Namen {18} = 'Gabi Meien';

% Nach einem Jahr hat Frau Meyer ihren Nachnamen zu "Meyer-Vorfelder"
% geaendert und konnte zwei Freunde von ihr fuer den Kurs begeistern. Der
% Kurs hatte somit 20 Teilnehmer

Namen {14} = 'Maria Meyer-Vorfelder';
Namen {19} = 'Doris Laub-Mair';
Namen {20} = 'Maier Shultheiss';

% b) Ueberpruefen Sie, ob die von Ihnen oben ermittelte Loesung weiterhin
% gueltig ist. Wenn dies nicht der Fall ist, fuehren Sie Korrekturen ein.

%% L??sung
close all;
clc;

%% Aufgabe
display('Aufgabe')
nameExpression = '\w*(\s|-)M(eie|eye|aie|aye|ay|ai)r'
positions = regexp(Namen, nameExpression);

display('Folgende Teilnehemer zur Variante Meyer wurden gefunden:')
disp(' ')
disp(Namen(~cellfun('isempty',positions))')
