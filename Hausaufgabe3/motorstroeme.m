function [ i1, i2, varargout ] = motorstroeme( M1, M2, I, varargin )

% varargin und varargout sind für die Funktion eigentlich nicht nötig. 
% Diese werden verwendet, da in der Aufgabenstellung gefordert ist auch 
% bei zu vielen Argumenten mit einer waring abzubrechen. Ohne varargin und
% varargout würde die Funktion mit einem Fehler beendet werden, da die
% maximale Argumentenzahl fix ist.

i1 = [];
i2 = [];
varargout = cell(1,1);

if nargin~=3
    warning('Pleas hand over exactly three arguments: M1, M2, I')
    return
end
% Wir überprüfen zusätzlich ob es sich bei dem skalaren Eingabeargument um 
% eine Zahl handelt. Dies wurde in der Aufgabenstellung vergessen.
if ~(isscalar(M1) && isnumeric(M1))
    warning('M1 has to be a scalar number')
    return
elseif ~(isscalar(M2) && isnumeric(M2))
    warning('M2 has to be a scalar number')
    return
elseif ~(isscalar(I) && isnumeric(I))
    warning('I has to be a scalar number')
    return
end
if nargout > 2
    warning('Too many outputarguments. Maximum is 2')
end
if M1==0 && M2==0
    warning('M1 = 0 and M2 = 0. 0/0 = NaN is not allowed.')
    return
end

i1 = I / (1+(M2/M1));
i2 = I / (1+(M1/M2));

if nargout == 0
    disp(['i1 = ', num2str(i1)])
    disp(['i2 = ', num2str(i2)])
elseif nargout == 1
    disp(['i2 = ', num2str(i2)])
end

end