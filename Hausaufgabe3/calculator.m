function calculator()
%% The best calculator!!11 (if it could handle decimals)
%% Settings
size = [500 340];

%% Calculator Setup
% Figure Settings
f = figure;
f.Name = 'Taschenrechner';
f.NumberTitle = 'off';
f.MenuBar = 'none';
f.ToolBar = 'none';
f.Resize = 'off';
f.Position(3:4) = size;

% Drawing Equation and Result Displays
displayLayout = getPositionMatrixForIndex(1,5,size,3);
displayLayout(4) = displayLayout(4)/2;
equationLayout = displayLayout;
equationLayout(2) = equationLayout(2) + displayLayout(4);

resultEdit = uicontrol('Style', 'edit', 'enable', 'off',...
    'FontSize', 20.0,...
    'Position', displayLayout);

equationEdit = uicontrol('Style', 'edit', 'enable', 'off',...
    'FontSize', 20.0,...
    'Position', equationLayout);

% Keyboard Callback
f.WindowKeyPressFcn = @(source,event) windowButtonDownCallback(equationEdit, resultEdit, source, event);

% Drawing buttons
strings = getButtonTitleStringsCellArray();

for row = 1:5
    for column = 1:5
        buttonwidth = 1;
        if (column == 5 && row < 4) || (column == 1 && row == 5)
            continue;
        end;
        if column == 1 && row == 4
            buttonwidth = 2;
        end;
        uicontrol('Style','pushbutton',...
            'String',strings{row, column},...
            'FontWeight', 'bold', 'FontSize', 16.0, ...
            'Position',getPositionMatrixForIndex(row,column,size,buttonwidth),...
            'Callback', @(source,event) buttonCallback(strings{row, column}, equationEdit, resultEdit, source, event));
    end
end

end

function windowButtonDownCallback(equationEdit,resultEdit,source,eventdata)
%% Callback function for Keyboard input 
% (Proxy/Converter Keyboard-input -> buttonCallback)
% - Checking if key is relevant
% - (Converting key, if needed)
% - Passing as button to buttonCallback

if strcmp(eventdata.Key, 'escape')
    button = 'CLR';
elseif strcmp(eventdata.Key, 'backspace')
    button = 'DEL';
elseif strcmp(eventdata.Key, 'return')
    button = '=';
elseif isscalar(str2num(eventdata.Character)) || ~isempty(strfind('()+-*/^', eventdata.Character)) %#ok<ST2NM>
    button = eventdata.Character;
else
    return
end

buttonCallback(button , equationEdit, resultEdit, source, eventdata);

end

function buttonCallback(button,equationEdit,resultEdit,~,~)
%% Callback function for UI-button actions.
% if special button: clear, delete or calculate
% else: append pressed key-input to equation-edit
if strcmp(button, 'CLR')
    equationEdit.String = '';
    resultEdit.String = '';
elseif strcmp(button, 'DEL')
    equationEdit.String = equationEdit.String(1:end-1);
elseif strcmp(button, '=')
    if isempty(equationEdit.String)
        return
    end
    try
        eval(['resultEdit.String = num2str(', equationEdit.String, ');']);
    catch
        warning('Syntax Error!')
    end
else
    equationEdit.String = [equationEdit.String, button];
end
end

function [position] = getPositionMatrixForIndex(row, column, size, width)
%% Provides the grid layout ([x y width height]) for the calculator
%% Settings
rows = 5;
columns = 5;
margin = [20,20]; % Inner distance of buttons to window's frame
padding = [5,5]; % Between Buttons

%% Calculation
% Size
inputIndex = [row, column];
layout = [rows, columns];
dimension = (size - 2*margin - (layout - 1).*padding) ./ layout;

% Position
paddingCount = inputIndex - 1;
coordinates = margin + (inputIndex - 1) .* dimension + paddingCount .* padding;

% Wide Buttons
width = width -1;
dimension(1) = dimension(1) + width * (dimension(1) + padding(1));

position = [coordinates, dimension];

end

function [strings] = getButtonTitleStringsCellArray()
%% Source for Button Titles
% (Implemented as 5x5 cell array of Strings)

strings = cell(5,5);
strings{1,1} = '';
strings{1,2} = '';
strings{1,3} = '';
strings{1,4} = 'DEL';
strings{1,5} = 'CLR';

strings{2,1} = '1';
strings{2,2} = '2';
strings{2,3} = '3';
strings{2,4} = '(';
strings{2,5} = ')';

strings{3,1} = '4';
strings{3,2} = '5';
strings{3,3} = '6';
strings{3,4} = '+';
strings{3,5} = '-';

strings{4,1} = '7';
strings{4,2} = '8';
strings{4,3} = '9';
strings{4,4} = '*';
strings{4,5} = '/';

strings{5,1} = '^';
strings{5,2} = '0';
strings{5,3} = 'sqrt(';
strings{5,4} = '=';
strings{5,5} = '=';

% Reshaping cell array from matrix enumeration to x-y-enumeration:
%   origin is in lower-left corner
%   x before y enumeration
strings = strings';
strings = strings(1:end, end:-1:1);
end