function [ combinations ] = komb2( varargin )

grids = cell(nargin,1);
[grids{:}] = ndgrid(varargin{:});

combinations = cat(3,grids{:});
combinations = reshape(combinations, numel(grids{1}), []);

end

